import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './screen/home/home.component';
import { AboutComponent } from './screen/about/about.component';
import { LoginComponent } from './screen/login/login.component';
import { ContactComponent } from './screen/contact/contact.component';
import { RegisterComponent } from './screen/register/register.component';
import { SharedComponent } from './shared/shared.component';
import { HeaderComponent } from './shared/header/header.component';
import { SlidebarComponent } from './shared/slidebar/slidebar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { PnfComponent } from './screen/pnf/pnf.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    LoginComponent,
    ContactComponent,
    RegisterComponent,
    SharedComponent,
    HeaderComponent,
    SlidebarComponent,
    FooterComponent,
    PnfComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [{provide:APP_BASE_HREF,useValue:""}],
  bootstrap: [AppComponent]
})
export class AppModule { }
