import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {HomeComponent} from "./screen/home/home.component";
import {AboutComponent} from "./screen/about/about.component";
import {ContactComponent} from "./screen/contact/contact.component";
import {LoginComponent} from "./screen/login/login.component";
import {RegisterComponent} from "./screen/register/register.component";
import {PnfComponent} from "./screen/pnf/pnf.component";

const routes: Routes = [
  {
    path : "",
    component : HomeComponent,
    pathMatch : "full"
  },
  {
    path : "home",
    component : HomeComponent
  },
  {
    path : "about",
    component : AboutComponent
  },
  {
    path : "contact",
    component : ContactComponent
  },
  {
    path : "login",
    component : LoginComponent
  },
  {
    path : "signup",
    component : RegisterComponent
  },
  {
    path : "home",
    component : HomeComponent
  },
  {
    path : "**",
    component : PnfComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
